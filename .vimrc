if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

" 執行 vim-plug
call plug#begin()

Plug 'tpope/vim-sensible'
Plug 'scrooloose/nerdcommenter'
Plug 'jpo/vim-railscasts-theme'
Plug 'bitterjug/vim-colors-bitterjug'

call plug#end()


syntax on
filetype plugin indent on

set encoding=utf8 nobomb

" search settings
set ic
set hls is
set smartcase

" bg color
set background=dark

" vim 共享 osx clipboard，可惜要升級 v7.4 才行
" set clipboard=unnamed

" clear last search -  其實直接打 :noh 就等於 nohighlight
" nnoremap <esc> :noh<return><esc>

" show line number
set number

" source ~/.vim/colors/railscasts.vim
" color jitterbug
silent! color railscasts

" NerdCommenter
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'

" 保持 tab 字元，不轉為 4 space
set softtabstop=0
set noexpandtab
"  if you want an indent to correspond to a single tab
set shiftwidth=4
set autoindent
set smartindent
set nowrap
set tabstop=4

" 實驗性質: 在 insert mode 時用 jj 當作 esc 以減少手指移動
inoremap jj <ESC>

" auto reload file when changed
set autoread

" start scrolling 7-lines before cursor hit the end
set so=7

