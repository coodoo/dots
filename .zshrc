# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
# ZSH_THEME="blinks"
ZSH_THEME="jx"

plugins=(git npm rsync docker docker-compose zsh-syntax-highlighting)

source $ZSH/oh-my-zsh.sh


# Customize to your needs...
alias sz='source ~/.zshrc'

# ls 最新的排最下面方便看
alias lsd='ls -later'

export PATH=/usr/local/bin:/usr/local/sbin:$PATH

# npm 列表
alias ng="npm list -g --depth=0 2>/dev/null"
alias nl="npm list --depth=0 2>/dev/null"

# 方便執行專案目錄下 node_modules/.bin/ 下面的程式
# 只要打 npm-exec babel-node 就好
alias npmexec='PATH=$(npm bin):$PATH'
alias nn='node $(node --v8-options | grep "harmony" | grep -Eo "(--\w+)")'
alias tu='top -u'

#==================================================
#
# docker 設定

alias dc='docker'
alias dcrmall='docker rm -f $(docker ps -aq)'
alias dcrmexited='docker rm $(docker ps -f "status=exited" -q)'
alias dcrmimg='docker rmi $(docker images -f "dangling=true" -q)'
alias dccomp='docker-compose' # 可直接打 dc version 就等於 docker-compose version
alias dcleave='docker swarm leave --force'
alias dcinit='docker swarm init'
alias dcmc='docker-machine'

# 管理 dotfiles
alias config='/usr/bin/git --git-dir=/root/.dotfiles/ --work-tree=/root'
